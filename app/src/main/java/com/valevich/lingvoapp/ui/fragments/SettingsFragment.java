package com.valevich.lingvoapp.ui.fragments;

import android.support.v4.app.Fragment;

import com.valevich.lingvoapp.R;

import org.androidannotations.annotations.EFragment;

@EFragment(R.layout.fragment_settings)
public class SettingsFragment extends Fragment {
}
